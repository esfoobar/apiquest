# APIQuest

### Description

A platform for a game that is played using API requests, where users can explore a virtually infinite world with planets, cities, and places, plant and find treasures and interact with characters built by players themselves.

### Components

* User: API users that administer the game
* Characters: Avatars of players in the game
* Activity: activity log across the game
* Geo: Groups the various location objects:
    * Planets
    * Cities
    * Roads
* Places: Houses, huts, etc., associated to a city or road
* Puppets: AI driven characters
* Objects: Things that a character can pick up or drop