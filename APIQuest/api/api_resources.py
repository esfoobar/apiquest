from tastypie.resources import ModelResource, ALL

from django.contrib.auth.models import User

class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        fields = ['username', 'first_name', 'last_name', 'last_login']
        filtering = {
            'username': ALL,
        }